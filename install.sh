#!/bin/bash
INSTALL_DIR=/opt/bashrc
sudo mkdir -p $INSTALL_DIR

echo "#!/bin/bash" > mybashrc.sh
find `pwd`/modules -type f -name '*.sh' | sort -V | while IFS= read -r FILE_PATH; do
    sudo cp $FILE_PATH $INSTALL_DIR
    FILE_NAME="${FILE_PATH##*/}"
    echo ". ${INSTALL_DIR}/${FILE_NAME}" >> mybashrc.sh
done

sudo mv mybashrc.sh $INSTALL_DIR
sudo cp $HOME/.zshrc $HOME/.zshrc.bak 2>/dev/null
sudo cp .zshrc $HOME

sed -i '/^\. \/opt\/bashrc\/mybashrc\.sh$/d' ${HOME}/.bashrc
echo ". /opt/bashrc/mybashrc.sh" >> ${HOME}/.bashrc
