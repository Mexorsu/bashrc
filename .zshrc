source ~/.bashrc
 function vim()
 {
     local STTYOPTS="$(stty --save)"
     stty stop '' -ixoff
     command vim "$@"
     stty "$STTYOPTS"
 }
plugins=(git mvn npm ng docker docker-compose java sudo)
export ZSH=$HOME/.oh-my-zsh
autoload -U zmv
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"


if [[ $1 == vim ]]
then
    ZSH_THEME="amuse"
    bindkey -v
else
    ZSH_THEME="agnoster"
fi
source $ZSH/oh-my-zsh.sh

SCRIPTS_DIR=$HOME/scripts

export PATH=$PATH:$SCRIPTS_DIR
alias save="git add .; git commit -m 'save'; git push origin master"
alias mmv='noglob zmv -W'
export ARCH=x86Linux

# HSTR configuration - add this to ~/.bashrc
alias hh=hstr                    # hh to be alias for hstr
export HISTFILE=~/.zsh_history  # ensure history file visibility
export HSTR_CONFIG=hicolor        # get more colors
bindkey -s "\C-r" "\eqhstr\n"     # bind hstr to Ctrl-r (for Vi mode check doc)

