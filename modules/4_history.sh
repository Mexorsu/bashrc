#!/bin/bash
HISTCONTROL=ignoreboth
HISTIGNORE='ls:bg:fg:history'
if [[ "$0" == "bash" ]]
then
    shopt -s histappend
fi
HISTSIZE=100000
HISTFILESIZE=20000000
PROMPT_COMMAND='history -a'
