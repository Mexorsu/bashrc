#!/bin/bash
find `pwd`/modules -type f -name '*.sh' | sort -V | while IFS= read -r line; do
    echo "$line"
done
