#!/bin/bash
# fix terminal resize bugs
if [[ "$0" == "bash" ]]
then
    shopt -s checkwinsize
fi

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


