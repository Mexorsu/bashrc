#!/bin/bash
. /opt/bashrc/1_utils.sh
. /opt/bashrc/2_colors.sh
. /opt/bashrc/3_gitaliases.sh
. /opt/bashrc/4_history.sh
. /opt/bashrc/5_vimstuff.sh
. /opt/bashrc/6_fixes.sh
. /opt/bashrc/7_aliases.sh
